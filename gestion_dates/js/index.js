

  // Sélectionnez l'élément où vous voulez afficher la date et l'heure actuelles
  const currentDateTimeElement = document.getElementById('currentDateTime');

  // Mettez à jour la date et l'heure toutes les secondes
  setInterval(() => {
    const now = dayjs(); // Obtenez la date et l'heure actuelles avec DayJS
    const formattedDateTime = now.format('DD/MM/YYYY HH:mm:ss'); // Formattez la date et l'heure selon le modèle souhaité
    currentDateTimeElement.textContent = formattedDateTime; // Mettez à jour le contenu de l'élément HTML avec la date et l'heure formatées
  }, 1000); // Mettez à jour toutes les secondes (1000 millisecondes)



  // Sélectionnez l'élément où vous voulez afficher le numéro de semaine et le jour
  const weekInfoElement = document.getElementById('weekInfo');

  // Mettez à jour le numéro de semaine et le jour toutes les secondes
  setInterval(() => {
    const now = dayjs(); // Obtenez la date et l'heure actuelles avec DayJS
    const weekNumber = now.week(); // Obtenez le numéro de semaine
    const dayName = now.format('dddd'); // Obtenez le nom du jour
    const weekInfoText = ` semaine ${weekNumber}  ${dayName}`; // Créez le texte à afficher
    weekInfoElement.textContent = weekInfoText; // Mettez à jour le contenu de l'élément HTML avec le numéro de semaine et le jour
  }, 1000); // Mettez à jour toutes les secondes (1000 millisecondes)


  // Sélectionnez l'élément où vous voulez afficher les dates de la semaine
  const weekDatesElement = document.getElementById('weekDates');

  // Mettez à jour les dates de la semaine toutes les secondes
  setInterval(() => {
    const now = dayjs(); // Obtenez la date et l'heure actuelles avec DayJS
    const startOfWeek = now.startOf('week'); // Obtenez le début de la semaine actuelle
    const endOfWeek = now.endOf('week'); // Obtenez la fin de la semaine actuelle
    const formattedStartOfWeek = startOfWeek.format('DD/MM/YYYY'); // Formattez la date de début de la semaine
    const formattedEndOfWeek = endOfWeek.format('DD/MM/YYYY'); // Formattez la date de fin de la semaine
    const weekDatesText = `Semaine du ${formattedStartOfWeek} au ${formattedEndOfWeek}`; // Créez le texte à afficher
    weekDatesElement.textContent = weekDatesText; // Mettez à jour le contenu de l'élément HTML avec les dates de la semaine
  }, 1000); // Mettez à jour toutes les secondes (1000 millisecondes)

  // Sélectionnez l'élément input et les éléments de compte à rebours
  const dateInput = document.getElementById('dateInput');
  const countdownMessage = document.getElementById('countdownMessage');
  const countdownTimer = document.getElementById('countdownTimer');

// Fonction de calcul du compte à rebours
function calculateCountdown() {
    const selectedDate = new Date(dateInput.value); // Obtenez la date sélectionnée depuis l'input
    const currentDate = new Date(); // Obtenez la date actuelle
  
    // Vérifiez si la date sélectionnée est postérieure à la date actuelle
    if (selectedDate > currentDate) {
      const timeDifference = selectedDate - currentDate; // Calcul de la différence de temps en millisecondes
      const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24)); // Calcul du nombre de jours
      const hours = Math.floor((timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)); // Calcul du nombre d'heures
      const minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60)); // Calcul du nombre de minutes
      const seconds = Math.floor((timeDifference % (1000 * 60)) / 1000); // Calcul du nombre de secondes
  
      // Mettre à jour les valeurs dans le tableau HTML
      document.getElementById('days').textContent = days;
      document.getElementById('hours').textContent = hours;
      document.getElementById('minutes').textContent = minutes;
      document.getElementById('seconds').textContent = seconds;
  
      // Affichage du compte à rebours
      countdownMessage.textContent = "Compte à rebours jusqu'à la date sélectionnée :";
    } else {
      // Affichage du message d'erreur si la date sélectionnée est antérieure ou égale à la date actuelle
      countdownMessage.textContent = 'Veuillez sélectionner une date postérieure à aujourd\'hui.';
      // Réinitialisation des valeurs du compte à rebours
      document.getElementById('days').textContent = '';
      document.getElementById('hours').textContent = '';
      document.getElementById('minutes').textContent = '';
      document.getElementById('seconds').textContent = '';
    }
  }
  
  // Fonction de mise à jour du compte à rebours
  function updateCountdown() {
    calculateCountdown(); // Calculer le compte à rebours
  
    // Mettre à jour le compte à rebours toutes les secondes
    setInterval(() => {
      calculateCountdown(); // Calculer à nouveau le compte à rebours
    }, 1000);
  }
  
  // Ajouter un écouteur d'événement pour détecter les changements dans l'input
  dateInput.addEventListener('change', updateCountdown);
  






  //Le calendrier 
  const currentMonthElement = document.getElementById('currentMonth');
  const calendarBodyElement = document.getElementById('calendarBody');
  const prevMonthBtn = document.getElementById('prevMonthBtn');
  const nextMonthBtn = document.getElementById('nextMonthBtn');
  
  // Liste des noms de mois
  const monthNames = [
    'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
    'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
  ];
  
  // Écouteurs d'événements pour les boutons précédent et suivant
  prevMonthBtn.addEventListener('click', showPreviousMonth);
  nextMonthBtn.addEventListener('click', showNextMonth);
  
  // Date actuelle
  let currentDate = new Date();
  
  // Afficher le calendrier pour le mois en cours
  showCalendar(currentDate.getFullYear(), currentDate.getMonth());
  
  // Fonction pour afficher le calendrier pour un mois spécifique
  function showCalendar(year, month) {
    // Mise à jour du mois affiché dans l'en-tête
    currentMonthElement.textContent = `${monthNames[month]} ${year}`;
  
    // Effacer le contenu du calendrier
    calendarBodyElement.innerHTML = '';
  
    // Obtenir le premier jour du mois
    const firstDay = new Date(year, month, 1);
    const startDay = getStartDayIndex(firstDay);
  
    // Obtenir le nombre de jours dans le mois
    const totalDays = getDaysInMonth(year, month);
  
    // Créer les cases du calendrier
    let day = 1;
    for (let row = 0; row < 6; row++) {
      const newRow = document.createElement('tr');
      for (let col = 0; col < 7; col++) {
        if ((row === 0 && col < startDay) || day > totalDays) {
          // Case vide avant le premier jour du mois ou après le dernier jour du mois
          const newCell = document.createElement('td');
          newRow.appendChild(newCell);
        } else {
          // Case avec le jour du mois
          const newCell = document.createElement('td');
          newCell.textContent = day;
          newCell.addEventListener('click', highlightDay);
          newRow.appendChild(newCell);
          day++;
        }
      }
      calendarBodyElement.appendChild(newRow);
    }
  }
  
  // Fonction pour afficher le mois précédent
  function showPreviousMonth() {
    currentDate.setMonth(currentDate.getMonth() - 1);
    showCalendar(currentDate.getFullYear(), currentDate.getMonth());
  }
  
  // Fonction pour afficher le mois suivant
  function showNextMonth() {
    currentDate.setMonth(currentDate.getMonth() + 1);
    showCalendar(currentDate.getFullYear(), currentDate.getMonth());
  }
  
  // Fonction pour obtenir l'indice du jour de la semaine (0-6)
  function getStartDayIndex(date) {
    const dayIndex = date.getDay();
    return dayIndex === 0 ? 6 : dayIndex - 1;
  }
  
  // Fonction pour obtenir le nombre de jours dans un mois
  function getDaysInMonth(year, month) {
    return new Date(year, month + 1, 0).getDate();
  }
  
  // Fonction pour mettre en évidence le jour sélectionné
  function highlightDay(event) {
    const selectedDay = event.target;
    // Supprimer la mise en évidence de tous les autres jours
    const allDays = calendarBodyElement.querySelectorAll('td');
    allDays.forEach(day => day.classList.remove('highlight'));
    // Mettre en évidence le jour sélectionné
    selectedDay.classList.add('highlight');
  }
  